
! inlist_brown_dwarf


&star_job
      show_log_description_at_start = .false.
      
!      create_pre_main_sequence_model = .true.
!      pre_ms_T_c = 2d5
!      pre_ms_logT_surf_limit = 2.7
!      pre_ms_logP_surf_limit = 8.0
!      pre_ms_relax_num_steps = 1000
      
      !! OUTPUT
      save_model_when_terminate = .true.
      save_model_filename = 'PL_A007.mod'
      
      load_saved_model = .true.
      saved_model_name = 'PL_A006.mod'
      
      change_net = .true.      
      !new_net_name = 'pp_extras.net'
      new_net_name = 'planet.net'
      
      !log_columns_file = 'log_columns.list' 
      !profile_columns_file = 'profile_columns.list' 
      
!      set_uniform_initial_composition = .true.
!      initial_h1 = 0.71
!      initial_h2 = 2d-5 ! if no h2 in current net, then this will be added to h1
!      initial_he3 = 2d-5
!      initial_he4 = 0.27
!      initial_zfracs = 3 ! GS98_zfracs = 3
      
!     file_for_uniform_xa = 'composition2.txt'
!      set_uniform_initial_xa_from_file = .true.
      
      num_steps_to_relax_composition = 100
      relax_initial_composition = .true.
      relax_composition_filename = 'composition_profile7.txt'      
      
!      eos_file_prefix = 'mesa'
!      kappa_file_prefix = 'gs98'
      
!      relax_mass_scale = .true.
!      dlgm_per_step = 1d-3 ! log10(delta M/Msun/step).
!      new_mass = 1.0
!      change_mass_years_for_dt = 1
      
      
!      change_lnPgas_flag = .true.
      new_lnPgas_flag = .true.
      
      !pgstar_flag = .true.

      mesa_dir = '/Users/neil/research/mesa'
      show_eqns_and_vars_names = .true.
      report_retries = .true.
      report_backups = .true.
/ ! end of star_job namelist

&controls

!      show_mesh_changes = .true.

      mesh_delta_coeff = 0.5
	
      use_lnE_for_eps_grav = .true.


      ! check for retries and backups as part of test_suite
      max_number_backups = 400
      max_number_retries = 1000

!      initial_mass = 0.05
!      initial_z = 0.02d0

      
      mixing_length_alpha = 1.89d0 ! based on solar model
      use_Henyey_MLT = .true.

      which_atm_option = 'tau_100_tables'
      
      photostep = 100
      profile_interval = 100
      history_interval = 100
      terminal_cnt = 10
      write_header_frequency = 10
      
      max_age = 5d5
      max_years_for_timestep = 1d3
      
      !! The default here is 1d-4 - would that be better for convergence?
      varcontrol_target = 1d-4
      max_model_number = 5000

      !report_hydro_solver_progress = .true. ! set true to see info about newton iterations
      !report_ierr = .true. ! if true, produce terminal output when have some internal error

      !report_why_dt_limits = .true.
      !report_all_dt_limits = .true.
      
      !show_mesh_changes = .true.
      !mesh_dump_call_number = 8242
      !okay_to_remesh = .false.

      trace_evolve = .true.
      
      ! hydro debugging
      !hydro_numerical_jacobian = .true.
      !hydro_check_everything = .true.
      !hydro_show_rcond = .true.
      !hydro_inspectB_flag = .true.
      !hydro_save_numjac_plot_data = .true.
      !hydro_dump_call_number = 24765
      
      RGB_wind_scheme = ''
      AGB_wind_scheme = ''
      RGB_to_AGB_wind_switch = 1d-4
      Reimers_wind_eta = 0.5d0  
      Blocker_wind_eta = 0.1d0  


/ ! end of controls namelist


&pgstar

         pause = .false. 
         ! if true, the code waits for user to enter a RETURN on the command line

      ! "main" window
      
         MAIN_win_flag = .true.
      
         show_HR_TRho_with_main = .true. ! if true, use aspect ratio < 1
         MAIN_win_width = 10
         MAIN_win_aspect_ratio = 0.78 ! aspect_ratio = height/width
         
         !show_HR_TRho_with_main = .false. ! if false, use aspect ratio > 1
         !MAIN_win_width = 8.5
         !MAIN_win_aspect_ratio = 1.2 ! aspect_ratio = height/width
      
         xaxis_by = 5 ! select xaxis for main window
            ! 1 = by_mass
            ! 2 = by_grid
            ! 3 = by_radius
            ! 4 = by_logR
            ! 5 = by_logP
            ! 6 = by_logxm
            
         show_main_win_text_info = .true.
         
         ! xaxis limits -- to override system default selections
         main_xmin = -101 ! only used if > -100
         main_xmax = -101 ! only used if > -100
         
         logxq_cutoff = -7.1 ! min value when using logxm for xaxis
         
         ! control for plot showing abundances
         log_mass_frac_ymax = 0.5 ! making this > 0 helps readability
         log_mass_frac_ymin = -3.3
         num_abundance_line_labels = 5
      
         log_abund_vary = 0.1 ! don't show species if it would vary by < this in plot

         show_main_annotation1 = .false.
         show_main_annotation2 = .false.
         show_main_annotation3 = .false.
         
         ! file output
         MAIN_file_flag = .false.
         MAIN_file_dir = 'pgstar_out'
         MAIN_file_prefix = 'main'
         MAIN_file_cnt = 5 ! output when mod(model_number,main_file_cnt)==0
         MAIN_file_width = 9 ! negative means use same value as for window
         MAIN_file_aspect_ratio = -1 ! negative means use same value as for window
         
         
      ! TRho Profile window -- current model in T-Rho plane
      
         TRho_Profile_win_flag = .true.

         TRho_Profile_win_width = 10
         TRho_Profile_win_aspect_ratio = 0.62 ! aspect_ratio = height/width
            
         show_TRho_Profile_text_info = .true.
         TRho_Profile_text_info_xfac = 0.81 ! controls x location
         TRho_Profile_text_info_dxfac = 0.02 ! controls x spacing to value from text
         TRho_Profile_text_info_yfac = 0.49 ! controls y location of 1st line
         TRho_Profile_text_info_dyfac = -0.04 ! controls line spacing
         
         show_TRho_Profile_legend = .true.
         TRho_Profile_legend_coord = 0.05
         TRho_Profile_legend_fjust = 0.0
         TRho_Profile_legend_disp1 = -2.5
         TRho_Profile_legend_del_disp = -1.5
         
         show_HR_TRho_with_TRho_Profile = .false.
         
         show_TRho_Profile_burn_labels = .true.
         
         show_TRho_Profile_cross_hair = .false.
         TRho_Profile_show_logQ_limit = .false.

         show_TRho_Profile_annotation1 = .false.
         show_TRho_Profile_annotation2 = .false.
         show_TRho_Profile_annotation3 = .false.
      
         ! axis limits
         TRho_Profile_xmin = -11.1
         TRho_Profile_xmax = 10.1
         TRho_Profile_ymin = 2.6
         TRho_Profile_ymax = 9.5         
         
         ! file output
         TRho_Profile_file_flag = .false.
         TRho_Profile_file_dir = 'pgstar_out'
         TRho_Profile_file_prefix = 'trho_profile'
         TRho_Profile_file_cnt = 5 ! output when mod(model_number,TRho_Profile_file_cnt)==0
         TRho_Profile_file_width = -1 ! negative means use same value as for window
         TRho_Profile_file_aspect_ratio = -1 ! negative means use same value as for window
      
      
      ! Profile window
      
         Profile_win_flag = .true.

         Profile_win_width = 9
         Profile_win_aspect_ratio = 0.62 ! aspect_ratio = height/width
            
         show_Profile_legend = .true.
         Profile_legend_coord = 0.05
         Profile_legend_fjust = 0.0
         Profile_legend_disp1 = -28.5
         Profile_legend_del_disp = -1.5
         Profile_show_mass_locs = .true.
         Profile_show_decorated_line = .true.
         
         show_HR_TRho_with_Profile = .true.
         show_Profile_cross_hair = .false.

         show_Profile_text_info = .true.
         Profile_text_info_xfac = 0.78 ! controls x location
         Profile_text_info_dxfac = 0.02 ! controls x spacing to value from text
         Profile_text_info_yfac = 0.94 ! controls y location of 1st line
         Profile_text_info_dyfac = -0.07 ! controls line spacing

         show_Profile_annotation1 = .false.
         show_Profile_annotation2 = .false.
         show_Profile_annotation3 = .false.
         
         ! axis choices -- use names from mesa/data/star_data/profile_columns.list
         ! you can also use element names or reaction category names
         ! here is a partial list of the available choices:
            ! mass, logT, logRho, logP, logR, entropy, csound, log_opacity, 
            ! eta, mu, grada, gradr, tau, chiRho, abar, ye, luminosity, ....
            ! h1, he3, c12, ...
            ! pp, cno, tri_alfa, .... (see rates/public/rates_def.f)
         Profile_xaxis_name = 'mass'
         Profile_xaxis_reversed = .false.
         Profile_xmin = -101 ! only used if > -100
         Profile_xmax = -101 ! only used if > -100
         
         Profile_yaxis_name = 'logT'
         Profile_yaxis_reversed = .false.
         Profile_ymin = -101 ! only used if > -100
         Profile_ymax = -101 ! only used if > -100        
         Profile_dymin = -1 
         
         Profile_other_yaxis_name = '' 
         Profile_other_yaxis_reversed = .false.
         Profile_other_ymin = -101 ! only used if > -100
         Profile_other_ymax = -101 ! only used if > -100
         Profile_other_dymin = -1 
         
         ! file output
         Profile_file_flag = .false.
         Profile_file_dir = 'pgstar_out'
         Profile_file_prefix = 'profile'
         Profile_file_cnt = 5 ! output when mod(model_number,Profile_file_cnt)==0
         Profile_file_width = -1 ! negative means use same value as for window
         Profile_file_aspect_ratio = -1 ! negative means use same value as for window

/ ! end of pgstar namelist

