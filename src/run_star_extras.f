! ***********************************************************************
!
!   Copyright (C) 2010  Bill Paxton
!
!   this file is part of mesa.
!
!   mesa is free software; you can redistribute it and/or modify
!   it under the terms of the gnu general library public license as published
!   by the free software foundation; either version 2 of the license, or
!   (at your option) any later version.
!
!   mesa is distributed in the hope that it will be useful, 
!   but without any warranty; without even the implied warranty of
!   merchantability or fitness for a particular purpose.  see the
!   gnu library general public license for more details.
!
!   you should have received a copy of the gnu library general public license
!   along with this software; if not, write to the free software
!   foundation, inc., 59 temple place, suite 330, boston, ma 02111-1307 usa
!
! ***********************************************************************
 
      module run_star_extras

      use star_lib
      use star_def
      use const_def
      use num_lib, only: safe_log10
      use mixeos_lib
      
      implicit none
      
      integer, parameter :: num_ages = 5
      integer :: age_cnt
      double precision, dimension(num_ages) :: ages, loggs, Teffs
      double precision, dimension(num_ages) :: target_loggs, target_Teffs
      double precision :: prev_age, prev_logg, prev_Teff
      
      
      integer :: time0, time1, clock_rate
      double precision, parameter :: expected_runtime = 0.5 ! minutes


      contains

      
      subroutine extras_controls(s, ierr)
         type (star_info), pointer :: s
         integer, intent(out) :: ierr
         real(dp) :: mu
         character (len=256) :: name
         
         ierr = 0

         write(*,*) "Setting custome eos => mixeos"
         s% other_eosDT_get              => mixeos_get
         s% other_eosDT_get_T            => mixeos_get_t
         s% other_eosDT_get_Rho          => mixeos_get_rho
         s% other_eosPT_get              => mixeosPT_get
         s% other_eosPT_get_T            => mixeosPT_get_T
         s% other_eosPT_get_Pgas         => mixeosPT_get_Pgas
         s% other_eosPT_get_Pgas_for_Rho => mixeosPT_get_Pgas_for_Rho
         s% use_other_eos = .true.
         

         name = "water"
         mu = 18.d0
         call mixeos_add_ideal(mu, name)
         write(*,*) "[run_star_extras] Adding ideal analytic water"
      end subroutine extras_controls
      
      
      integer function extras_startup(s, id, restart, ierr)
         type (star_info), pointer :: s
         integer, intent(in) :: id
         logical, intent(in) :: restart
         integer, intent(out) :: ierr
         ierr = 0
         extras_startup = 0
         call system_clock(time0,clock_rate)
         age_cnt = 0
         prev_age = 0
         ages(:) = (/ 1d-3, 1d-2, 1d-1, 1d0, 1d1 /) ! Gyrs
         target_loggs(:) = (/ 3.5156379d0, 3.9779386d0, 4.545764d0, 4.72135d0, 4.797736d0 /)
         target_Teffs(:) = (/ 2.463198d3, 2.41337242d3, 1.5603d3, 7.80458d2, 4.07689d2 /)
      end function extras_startup
      

      ! returns either keep_going, retry, backup, or terminate.
      integer function extras_check_model(s, id, id_extra)
         type (star_info), pointer :: s
         integer, intent(in) :: id, id_extra
         extras_check_model = keep_going         
      end function extras_check_model


      integer function how_many_extra_history_columns(s, id, id_extra)
         type (star_info), pointer :: s
         integer, intent(in) :: id, id_extra
         how_many_extra_history_columns = 0
      end function how_many_extra_history_columns
      
      
      subroutine data_for_extra_history_columns(s, id, id_extra, n, names, vals, ierr)
         type (star_info), pointer :: s
         integer, intent(in) :: id, id_extra, n
         character (len=maxlen_history_column_name) :: names(n)
         real(dp) :: vals(n)
         integer, intent(out) :: ierr
         ierr = 0
      end subroutine data_for_extra_history_columns

      
      integer function how_many_extra_profile_columns(s, id, id_extra)
         type (star_info), pointer :: s
         integer, intent(in) :: id, id_extra
         how_many_extra_profile_columns = 0
      end function how_many_extra_profile_columns
      
      
      subroutine data_for_extra_profile_columns(s, id, id_extra, n, nz, names, vals, ierr)
         type (star_info), pointer :: s
         integer, intent(in) :: id, id_extra, n, nz
         character (len=maxlen_profile_column_name) :: names(n)
         real(dp) :: vals(nz,n)
         integer, intent(out) :: ierr
         integer :: k
         ierr = 0
      end subroutine data_for_extra_profile_columns
      

      ! returns either keep_going, retry, backup, or terminate.
      integer function extras_finish_step(s, id, id_extra)
         type (star_info), pointer :: s
         integer, intent(in) :: id, id_extra
         extras_finish_step = keep_going
         call check_age(s)
         prev_age = s% star_age
         prev_logg = safe_log10(s% cgrav(1)*s% mstar/(s% r(1)**2))
         prev_Teff = s% Teff
      end function extras_finish_step
      
      
      subroutine check_age(s)
         type (star_info), pointer :: s
         double precision :: next_age, alfa, logg, Teff
         include 'formats'
         if (age_cnt == num_ages) return
         next_age = ages(age_cnt+1)*1d9 ! years
         if (s% star_age < next_age) return
         age_cnt = age_cnt+1
         alfa = (next_age - prev_age)/(s% star_age - prev_age)
         logg = safe_log10(s% cgrav(1)*s% mstar/(s% r(1)**2))
         Teff = s% Teff
         loggs(age_cnt) = alfa*logg + (1-alfa)*prev_logg
         Teffs(age_cnt) = alfa*Teff + (1-alfa)*prev_Teff
         write(*,2) 'logg Teff', age_cnt, loggs(age_cnt), Teffs(age_cnt)
         write(*,*)
      end subroutine check_age
      
      
      subroutine extras_after_evolve(s, id, id_extra, ierr)
         type (star_info), pointer :: s
         integer, intent(in) :: id, id_extra
         integer, intent(out) :: ierr
         double precision :: dlogg, dTeff, tol, dt
         logical :: aok
         integer :: i
         include 'formats'
         ierr = 0
         if (s% model_number < 20) return
         call check_age(s)
         write(*,'(a6,5a20)') 'i', 'age', 'logg', 'Teff', 'dlogg', 'dTeff'
         aok = .true. 
         tol = 5d-3
         do i=1,age_cnt
            dlogg = (loggs(i) - target_loggs(i)) / target_loggs(i)
            dTeff = (Teffs(i) - target_Teffs(i)) / target_Teffs(i)
            if (abs(dlogg) > tol .or. abs(dTeff/Teffs(i)) > tol) aok = .false.
            write(*,'(i6,8x,5f20.10)') i, ages(i), loggs(i), Teffs(i), dlogg, dTeff
         end do
         write(*,*)
         if (aok) then
            write(*,'(a)') 'all values are within tolerance'
         else
            write(*,'(a)') 'FAILED -- some values too far from target'
         end if
         call system_clock(time1,clock_rate)
         dt = dble(time1 - time0) / clock_rate / 60
         if (dt > 10*expected_runtime) then
            write(*,'(/,a30,2f18.6,a,/)') '>>>>>>> EXCESSIVE runtime', &
               dt, expected_runtime, '   <<<<<<<<<  ERROR'
         else
            write(*,'(/,a30,2f18.6,2i10/)') 'runtime, retries, backups', &
               dt, expected_runtime, s% num_retries, s% num_backups
         end if
      end subroutine extras_after_evolve

      end module run_star_extras
      
